//This text is a standin I used for testing
//In almost all cases this text will likely be sent down to the client server-side
//code (optimal scenario) or it will be set by some action on
//the client which may make ajax calls to get the searchable text
//either from the site the search feature is implemented on or some
//third party site, however the third party site will likely need
//to have the proper CORS Headers in place for that to work (if that's the route
//you are taking)

const text = `a whole lot of text goes here, usually. Like you know
a bunch of text that needs to be searched and what not. So, like perhaps you
may want to put a bunch of text here somehow or what good is this little
program anyhow? See What I mean? Ok, now go make something cool, and if
you have time, definitely show me. I look forward to seeing 
what you come up with and I will talk to you later. Thanks!`

//Here we have an array which we want to keep track of strings
//that we want to replace in our input string (string above)
//and also what we want it to be replaced with
//In the remove puncuation function (remPunc) we will iterate
//through this puncArr (array of objects) with a forEach
//removing the need for a confusing and ugly gigantic chain
//of .replace(del,add) methods, further confusing a potential analyst/coder
const puncArr = [
  { txt: /[!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]/g, sub: "" }, //general puncuation
  { txt: /[\n]/g, sub: " " } //line break
]
    
const remPunc = (str) => {
  var result = text
  
  //for each object in the array, replace the txt prop value in the arr
  //with the sub prop value
  puncArr.forEach( (obj) => {

		result = result.replace(obj.txt,obj.sub) 

	})
  
  return result
}

//remove duplicates by taking advantage of the new Set function
//which only takes unique values from an array to create a set
//but then we have to convert it back to an array with Array.from
const remDups = (arr) => Array.from(new Set(arr))

//create array from string adding a new array value
//for each value between spaces in the string
var textArr = remPunc(text).split(' ')

//This is a test query
var query = 'at'

//instantiate empty object to use in the scope of
//the function used to generate the matches variable value
var rankedMatches = []
var matches = textArr.filter(word => {
 
  var found = false
 
  //If the word is found then let's
  //keep track of how many times
  //the query is found so we can rank them
  //by importance assuming frequency is high value
  //a useful paradigm to start with
  if(word.includes(query)){
       if(rankedMatches[word]){
           rankedMatches
       } else {
           rankedMatches[word] = 1
       }
    			  //whether or not the word has been previously found and
           //its frequency previously recorded in our rankedMatches array
           //The word does include the query, so we return true in order
           //to signal the filter function that we want to include this 
           //current word in the resulting array of words in which the query is found
           return true
  }
 
    //Here we have failed the word includes query test
    //so since the query isn't in our current iterative word
    //we signal the filter function's return value as a false
    // so that we do not output this current word to the resulting array
    return false
})

rankedMatches.sort((a,b)=>a.

var filtered = remDups(matches)

filtered

